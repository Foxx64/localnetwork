<?xml version="1.0"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
targetNamespace="http://neoflex.ru/example"
xmlns="http://neoflex.ru/example"
elementFormDefault="qualified">
<xs:element name="computer">
 <xs:complexType>
  <xs:sequence> 
    <xs:element name="name" type="xs:string"/> 
    <xs:element name="osVersion" type="xs:string"/> 
    <xs:element name="ip" type="xs:string"/> 
    <xs:element name="id" type="xs:int"/> 
   </xs:sequence>
  </xs:complexType>
</xs:element> 
</xs:schema>