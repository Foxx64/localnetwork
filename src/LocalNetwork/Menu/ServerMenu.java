package LocalNetwork.Menu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import LocalNetwork.Interfaces.AbstractFactoryInterface;
import LocalNetwork.Entitys.Server;
import LocalNetwork.Exceptions.EntityNotFoundException;
import LocalNetwork.Interfaces.ServerServiceInterface;
import LocalNetwork.LokalNetwork;
import LocalNetwork.Utils.Utils;
import org.apache.log4j.Logger;

public class ServerMenu {
	static Logger logger = Logger.getLogger(LokalNetwork.class);

	public void serversMenu(Scanner scan) throws EntityNotFoundException, IOException {
		while(true) {
			System.out.println("Серверы: \n 1 - Открыть \n 2 - Создать " +
					"\n 3 - Показать список \n 0 - Выход \n");
			int serverAction = Utils.nextIntLine(scan);
			if (serverAction == 1){
				logger.debug("преход переход в меню openMenu");
				openMenu(scan);
			} else if (serverAction == 2){
				logger.debug("преход переход в меню createMenu");
				createMenu(scan);
			} else if (serverAction == 3){
				logger.debug("преход переход в меню printAll");
				printAll();
			} else if (serverAction == 0){
				logger.debug("выход из меню computersMenu");
				break;
			}
		}
	}

	public void printAll() throws IOException, EntityNotFoundException {
		ServerServiceInterface service = getServerService();
		ArrayList<Server> servers = service.getAll();
		if (servers.size() == 0){
			System.out.println("В сети нет ни одного сервера, добавьте пожалуйста сервер");
			logger.debug("выход из меню printAll");
		} else {
			System.out.println("Список серверов: ");
			for(int i = 0; i < servers.size(); i++){
				System.out.println("Имя сервера " + servers.get(i).getName() + " ");
				System.out.println("Версия ОС " + servers.get(i).getOsVersion() + " ");
				System.out.println("IP адрес " + servers.get(i).getIp() + " ");
				System.out.println("Параметры жесткого диска "  + servers.get(i).getStorage() + " ");
				System.out.println("ID " + servers.get(i).getId() + " ");
			}
		}
	}

	public void createMenu(Scanner scan) throws IOException, EntityNotFoundException {
		ServerServiceInterface service = getServerService();
		Server server = new Server();
		fill(scan, server);
		service.addServer(server);
	}

	public void openMenu(Scanner scan) throws EntityNotFoundException, IOException {
		ServerServiceInterface service = getServerService();
		ArrayList<Server> servers = service.getAll();
		if (servers.size() != 0){
			System.out.println("Введите пожалуйста имя сервера или ID");
			String search = scan.nextLine();
			if(Utils.tryParseInt(search))
			{
				Integer id = Integer.parseInt(search);
				Server server = service.getServerById(id);
				print(server);
				serversAction(scan, server);
			} else {
				Server server = service.getByName(search);
				print(server);
				serversAction(scan, server);
			}
		}else {
			System.out.println("В сети нет ни одного сервера, добавьте пожалуйста сервер");
			logger.debug("выход из меню openMenu");
		}
	}

	public void serversAction(Scanner scan, Server server) throws EntityNotFoundException, IOException {
		ServerServiceInterface service = getServerService();
		System.out.println("Действия с серверами: ");
		ServerServiceInterface svc = getServerService();
		while(true) {
			System.out.println("Серверы: \n 1 - Удалить \n 2 - Изменить \n 3 - Добавить компьютер \n 4 - Удалить компьютер \n 5 - Показать список компьютеров \n 0 - Выход ");
			int serverAction = Utils.nextIntLine(scan);
			if (serverAction == 1){
				logger.info("с");
				service.remove(server);
				System.out.println("Сервер удален! ");
			}else if (serverAction == 2){
				logger.info("изменение сервера");
				fill(scan, server);
				svc.updateServer(server);
			} else if (serverAction == 3){
				System.out.println("введите пожалуйста ID компьютера");
				int computerId = Utils.nextIntLine(scan);
				logger.info("добавление компьютера");
				service.addComputer (server, computerId);
				svc.updateServer(server);
			} else if (serverAction == 4){
				logger.info("удаление компьютера");
				service.removeComputer(server, scan);
			} else if (serverAction == 5){
				logger.info("вывод на экран списка компьютеров");
				printComputersList(server);
			} else if (serverAction == 0){
				logger.debug("выход из меню serverAction");
				break;
			}
		}
	}

	private void printComputersList(Server server) {
		if(server.serverComputers.size() == 0){
			System.out.println(" У сервера нет рабочих станций ");
			logger.debug("выход из меню printComputersList");
		}else {
			for (int i = 0; i < server.serverComputers.size(); i++){
		ComputerMenu cs = new ComputerMenu();
				cs.print(server.serverComputers.get(i));
			}
		}
	}

	private void fill(Scanner scan, Server server) throws IOException, EntityNotFoundException {
		System.out.println("Введите версию операционной системы");
		String serverOsVersion = scan.nextLine();
		server.setOsVersion(serverOsVersion);
		if (server.getOsVersion().equals(" "))
		{
			logger.error("ошибка при введении serverOsVersion");
			throw new IllegalArgumentException();
		}

		System.out.println("Введите имя сервера");
		String serverName = scan.nextLine();
		server.setName(serverName);
		if (server.getName().equals(" "))
		{
			logger.error("ошибка при введении serverName");
			throw new IllegalArgumentException();
		}

		System.out.println("Укажите параметры жесткого диска");
		String serverStorage = scan.nextLine();
		server.setStorage(serverStorage);
		if (server.getStorage().equals(" "))
		{
			logger.error("ошибка при введении serverStorage");
			throw new IllegalArgumentException();
		}

		System.out.println("Введите IP адрес");
		String serverIp = scan.nextLine();
		server.setIp(serverIp);
		if (server.getIp().equals(" "))
		{
			logger.error("ошибка при введении serverIp");
			throw new IllegalArgumentException();
		}
	}

	public void print(Server server) {
		System.out.println("Результаты поиска: ");
		System.out.println("Имя сервера " + server.getName() + " ");
		System.out.println("Версия ОС " + server.getOsVersion() + " ");
		System.out.println("IP адрес " + server.getIp() + " ");
		System.out.println("Параметры жесткого диска "  + server.getStorage() + " ");
		System.out.println("ID " + server.getId() + " ");
	}

	public static ServerServiceInterface getServerService(){
		AbstractFactoryInterface f = LokalNetwork.getFactory();
		return f.createServerService();
	}
}