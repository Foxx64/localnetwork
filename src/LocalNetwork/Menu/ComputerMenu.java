package LocalNetwork.Menu;
import LocalNetwork.Interfaces.*;
import LocalNetwork.Entitys.Computer;
import LocalNetwork.Exceptions.EntityNotFoundException;
import LocalNetwork.Interfaces.ComputerServiceInterface;
import LocalNetwork.LokalNetwork;
import LocalNetwork.Utils.Utils;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class ComputerMenu {
	static Logger logger = Logger.getLogger(ComputerMenu.class);
	public void computersMenu(Scanner scan) throws EntityNotFoundException, JAXBException, IOException {
		while(true) {
			System.out.println("Компьютеры: \n 1 - Открыть \n 2 - Создать \n 3 - Показать список \n 0 - Выход");
			int computerAction = nextIntLine(scan);
			if (computerAction == 1){
				logger.debug("преход переход в меню openMenu");
				openMenu(scan);
			} else if (computerAction == 2){
				logger.debug("преход переход в меню createMenu");
				createMenu(scan);
			} else if (computerAction == 3){
				logger.debug("преход переход в меню printAll");
				printAll();
			} else if (computerAction == 0){
				logger.debug("выход из меню computersMenu");
				break;
			}
		}
	}

	public void printAll() throws IOException {
		ComputerServiceInterface service = getComputerService();
		ArrayList <Computer> computers = service.getAll();
		if (computers.size() == 0){
			System.out.println("В сети нет ни одного компьютера, добавьте пожалуйста компьютер");
			logger.debug("выход из меню printAll");
		} else {
			System.out.println("Список компьютеров: ");
			for (Computer computer : computers) {
				System.out.println("Имя компьютера " + computer.getName() + " ");
				System.out.println("Версия ОС " + computer.getOsVersion() + " ");
				System.out.println("IP адрес " + computer.getIp() + " ");
				System.out.println("Параметры жесткого диска " + computer.getStorage() + " ");
				System.out.println("ID " + computer.getId() + " ");
			}
		}
	}

	public void createMenu(Scanner scan) throws JAXBException, IOException {
		Computer computer = new Computer();
		fill(computer,scan);
		ComputerServiceInterface service = getComputerService();
		service.addComputer(computer);
	}

	public void openMenu(Scanner scan) throws EntityNotFoundException {
		ComputerServiceInterface service = getComputerService();
		ArrayList<Computer> computers = service.getAll();
		if (computers.size() != 0){
			System.out.println("Введите пожалуйста имя компьютера или ID");
			String search = scan.nextLine();
			if(Utils.tryParseInt(search))
			{
				Integer id = Integer.parseInt(search);
				Computer computer = service.getComputerById(id);
				print(computer);
				computersAction(scan, computer);
			} else {
				Computer computer = service.getByName(search);
				print(computer);
				computersAction(scan, computer);
			}
		}else {
			System.out.println("В сети нет ни одного компьютера, добавьте пожалуйста компьютер");
			logger.debug("выход из меню openMenu");
		}
	}

	public void computersAction(Scanner scan, Computer computer ){
		ComputerServiceInterface service = getComputerService();

		while(true) {
			System.out.println("Действия с компьютерами: ");
			System.out.println("Компьютеры: \n 1 - Удалить \n 2 - Изменить \n 0 - Выход");
			int computerAction = nextIntLine(scan);
			if (computerAction == 1){
				logger.info("удаление компьютера");
				service.remove(computer);
				System.out.println("Компьютер удален! ");
			}else if (computerAction == 2){
				logger.info("изменение компьютера");
				fill(computer,scan);
				service.updateComputer(computer);
			}else if (computerAction == 0){
				logger.debug("выход из меню computersAction");
				break;
			}
		}
	}

	public void fill(Computer computer, Scanner scan){

			System.out.println("Создать компьютер: Введите версию операционно системы");
			String computerOsVersion = scan.nextLine();
			computer.setOsVersion(computerOsVersion);
			if (computer.getOsVersion().equals(" ")) {
				logger.error("ошибка при создании computerOsVersion");
				throw new IllegalArgumentException();

			}
			System.out.println("Введите имя компьютера");
			String computerName = scan.nextLine();
			computer.setName(computerName);
			if (computer.getName().equals(" ")) {
				logger.error("ошибка при создании computerName");
				throw new IllegalArgumentException();
			}
			System.out.println("Укажите параметры жесткого диска");
			String computerStorage = scan.nextLine();
			computer.setStorage(computerStorage);
			if (computer.getStorage().equals(" ")) {
				logger.error("ошибка при создании computerStorage");
				throw new IllegalArgumentException();
			}
			System.out.println("Введите IP адрес");
			String computerIp = scan.nextLine();
			computer.setIp(computerIp);
			if (computer.getIp().equals(" ")) {
				logger.error("ошибка при создании computerIp");
				throw new IllegalArgumentException();
			}
	}

	public void print(Computer computer) {
		System.out.println("Результаты поиска: ");
		System.out.println("Имя компьютера " + computer.getName() + " ");
		System.out.println("Версия ОС " + computer.getOsVersion() + " ");
		System.out.println("IP адрес " + computer.getIp() + " ");
		System.out.println("Параметры жесткого диска "  + computer.getStorage() + " ");
		System.out.println("ID " + computer.getId() + " ");
	}

	public int nextIntLine(Scanner scan){
		int tmp = scan.nextInt();
		scan.nextLine();
		return tmp;
	}

	public static ComputerServiceInterface getComputerService() {
		AbstractFactoryInterface f = LokalNetwork.getFactory();
		return f.createComputerService();
	}
}
