package LocalNetwork.Storage;

import LocalNetwork.Entitys.Computer;
import LocalNetwork.Entitys.Server;
import LocalNetwork.Exceptions.EntityNotFoundException;
import LocalNetwork.LokalNetwork;
import LocalNetwork.Services.StorageComputerService;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;


public class Storage {
	public ArrayList<Computer> computers = new ArrayList<Computer>();
	public ArrayList<Server> servers = new ArrayList<Server>();
	static Logger logger = Logger.getLogger(LokalNetwork.class);

	public static Storage instance = null;
	public static Storage getInstance() {
		if (instance == null) {
			synchronized (Storage.class) {
				if (instance == null) {
					instance = new Storage();
				}
			}
		}
		return instance;
	}

	public void readComputer () throws IOException {
//		Computer computer = new Computer();
//		computer.setIp("123.15.18.12");
//		computer.setName("Иванов Иван");
//		computer.setId(0);
//		computer.setOsVersion("Windows 2008");
//		computer.setStorage("5tb");
//		computers.add(computer);
		Storage.getInstance().computers = new ArrayList<Computer>();
		String computerPath = getComputerPath();
		byte[] bytes = java.nio.file.Files.readAllBytes(Paths.get(computerPath));
		ByteArrayInputStream is = new ByteArrayInputStream(bytes);
		ObjectInputStream ois = new ObjectInputStream(is);
		try {
			Storage.getInstance().computers = (ArrayList<Computer>) ois.readObject();
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		}
	}

	public void readServer() throws IOException, EntityNotFoundException {
//		Server server = new Server();
//		server.setIp("123.15.18.12");
//		server.setName("Titan");
//		server.setId(0);
//		server.setOsVersion("Windows 2008");
//		server.setStorage("5tb");
//		servers.add(server);
		String serverPath = getServerPath();
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(serverPath)));
		String wordsLine;
		while( ( wordsLine = in.readLine() ) != null  ){
			Server server = new Server();
			server.setName(wordsLine);
			wordsLine = in.readLine();
			server.setOsVersion(wordsLine);
			wordsLine = in.readLine();
			server.setStorage(wordsLine);
			wordsLine = in.readLine();
			server.setIp(wordsLine);
			wordsLine = in.readLine();
			int item = Integer.parseInt(wordsLine);
			server.setId(item);
			wordsLine = in.readLine();
			String[] ids = wordsLine.split(",");
			for (String string : ids) {
				if (string.equals("")){
					continue;
				}
				int number = Integer.parseInt(string);
				addComputer(server, number);
			}
			servers.add(server);
	}
		in.close();
	}	
	
	public static String getComputerPath() {
		URL location = LokalNetwork.class.getProtectionDomain().getCodeSource().getLocation();
		String binPath = null;
		try {
			File file = new File(location.toURI());
			binPath = file.getPath();
		} catch (URISyntaxException e) {
			logger.error("ошибка", e);
			e.printStackTrace();
		}
		String writeComputer = binPath + "\\..\\computer.txt";
		return writeComputer;
	}
	
	private static String getServerPath() {
		URL location = LokalNetwork.class.getProtectionDomain().getCodeSource().getLocation();
		String binPath = location.getFile();
		return binPath + "\\..\\server.txt";
	}
	
	private void addComputer(Server server, int computerId) throws EntityNotFoundException {
		StorageComputerService scs = new StorageComputerService();
		Computer computer = scs.getComputerById(computerId);
		computers.add(computer);
		server.addServerComputers(computer);
	}

	public static void computersWriteToFile(){
		Computer computer = new Computer();
		computer.setName("Ivanov");
		computer.setOsVersion("Windows 2008");
		computer.setIp("123.12.15.45");
		computer.setId(0);

		ArrayList<Computer> computers = Storage.getInstance().computers;
			String writeComputer = getComputerPath();
		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(os);
			oos.writeObject(computers);
			byte[] bytes = os.toByteArray();
			java.nio.file.Files.write(Paths.get(writeComputer), bytes);

		} catch (IOException e) {
			logger.error("ошибка", e);
		}
	}
	
	public static void serversWriteToFile(ArrayList<Server> servers){
		
		String writeServer = getServerPath();
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(writeServer));
			for (Server server : servers) {
				String line = server.getName() + "\r\n" + server.getOsVersion() + "\r\n" + server.getStorage()
						+ "\r\n" + server.getIp() + "\r\n" + server.getId() + "\r\n" + getComputersID(server) + "\r\n";
				bw.write(line);
			}
			bw.close();
		} catch (IOException e) {
			logger.error("ошибка", e);
		}
	}

	public  static String getComputersID(Server server){
		String string = "";
		for(int i = 0; i < server.serverComputers.size(); i++){
			if (i == server.serverComputers.size()-1){
				string = string + server.serverComputers.get(i).getId() + "";
			} else {
				string = string + server.serverComputers.get(i).getId()+ ",";
			}
		}
		return string;
	}
}
