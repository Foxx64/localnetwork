package LocalNetwork.Entitys;

import java.io.Serializable;
import java.util.ArrayList;

public class Server implements Serializable{
	private String name;
	private String osVersion;
	private String storage;
	private String ip;
	private Integer id;
	public ArrayList <Computer> serverComputers = new ArrayList<Computer>();
	
	public void addServerComputers (Computer computer){
		serverComputers.add(computer);
	}
	
	public void removeServerComputers (Computer computer){
		serverComputers.remove(computer);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getOsVersion() {
		return osVersion;
	}
	
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	
	public String getStorage() {
		return storage;
	}
	public void setStorage(String storage) {
		this.storage = storage;
	}
	
	public String getIp() {
		return ip;
	}
	
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}


}
