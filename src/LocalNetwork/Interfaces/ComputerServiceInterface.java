package LocalNetwork.Interfaces;

import LocalNetwork.Entitys.Computer;
import LocalNetwork.Exceptions.EntityNotFoundException;

import java.util.ArrayList;

public interface ComputerServiceInterface {
    ArrayList<Computer> getAll();
    void addComputer(Computer computer);
    Computer getComputerById(Integer id)  throws EntityNotFoundException;
    Computer getByName(String name);
    void remove(Computer computer);
    void updateComputer(Computer computer);
}
