package LocalNetwork.Interfaces;

import LocalNetwork.Interfaces.ComputerServiceInterface;
import LocalNetwork.Interfaces.ServerServiceInterface;

 public interface AbstractFactoryInterface {
    public ComputerServiceInterface createComputerService();
    public ServerServiceInterface createServerService();
  }
