package LocalNetwork.Interfaces;

import LocalNetwork.Entitys.Server;
import LocalNetwork.Exceptions.EntityNotFoundException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public interface ServerServiceInterface {
    ArrayList<Server> getAll() throws IOException, EntityNotFoundException;
    void addServer(Server server) throws IOException, EntityNotFoundException;
    Server getServerById(Integer id) throws EntityNotFoundException, IOException;
    Server getByName(String name) throws IOException, EntityNotFoundException;
    void remove(Server server);
    void removeComputer(Server server, Scanner scan) throws EntityNotFoundException;
    void addComputer(Server server, int computerId) throws EntityNotFoundException ;
    void updateServer(Server server);
}