package LocalNetwork.Utils;

import org.apache.log4j.Logger;

import java.util.Scanner;

public class Utils {
	static Logger logger = Logger.getLogger(Utils.class);

	public static int nextIntLine(Scanner scan){
		int tmp = scan.nextInt();
		scan.nextLine();
		return tmp; 
	}
	
	public static boolean tryParseInt(String value)
	{  
	     try  
	     {
			 Integer.parseInt(value);
	         return true;  
	      } catch(NumberFormatException nfe)

	      {
			  logger.error("Выход за границы массива");
	          return false;  
	      }  
	}
}
