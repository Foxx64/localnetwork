package LocalNetwork;
import java.io.IOException;
import java.util.Scanner;

import LocalNetwork.Exceptions.EntityNotFoundException;
import LocalNetwork.Factory.FileFactory;
import LocalNetwork.Factory.JdbcFactory;
import LocalNetwork.Factory.StorageFactory;
import LocalNetwork.Factory.XmlFactory;
import LocalNetwork.Interfaces.AbstractFactoryInterface;
import LocalNetwork.Menu.ComputerMenu;
import LocalNetwork.Menu.ServerMenu;
import LocalNetwork.Services.JdbcComputerService;
import LocalNetwork.Storage.Storage;
import org.apache.log4j.*;

public class LokalNetwork {

    static Logger logger = Logger.getLogger(LokalNetwork.class);
    public static int storeType = 0;

    public static void main(String[] args) throws IOException, EntityNotFoundException {
        initLog4J();
        JdbcComputerService.loadDriver();
        ComputerMenu cm = new ComputerMenu();
        Scanner scan = new Scanner(System.in);
        ServerMenu serverService = new ServerMenu();
        Storage.getInstance().readComputer();
        Storage.getInstance().readServer();

        while(true) try {
            System.out.println("Начало: \n 1 - Компьютеры \n 2 - Серверы \n" +
                    " 3 - Хранить в памяти \n " +
                    "4 - Хранить в файлах \n " +
                    "5 - Хранить компьютеры в XML файлах \n " +
                    "6 - Хранить компьютеры в базе данных \n 0 - Выход");
            String menuItem = scan.nextLine();
            if (menuItem.equals("0")) {
                logger.debug("преход выход из меню");
                Storage.computersWriteToFile();
                Storage.serversWriteToFile(Storage.getInstance().servers);
                break;
            } else if (menuItem.equals("1")) {
                cm.computersMenu(scan);
                logger.debug("переход в меню computerService");
            } else if (menuItem.equals("2")) {
                serverService.serversMenu(scan);
                logger.debug("перход в меню serversMenu");
            }
            else if (menuItem.equals("3")){
                storeType = 0;
            }
            else if (menuItem.equals("4")){
                storeType = 1;
            }
            else if (menuItem.equals("5")){
                storeType = 2;
            }
            else if (menuItem.equals("6")){
                storeType = 3;
            }
        } catch (Exception e) {
            logger.error("Ошибка" + e);
            e.printStackTrace();
        }
        scan.close();
    }

    private static void initLog4J() {
        FileAppender fa = new FileAppender();
        fa.setName("FileLogger");
        fa.setFile("myLog.log");
        fa.setLayout(new PatternLayout("%d %-5p [%c{1}] %m%n"));
        fa.setThreshold(Level.DEBUG);
        fa.setAppend(true);
        fa.activateOptions();
        Logger.getRootLogger().addAppender(fa);
    }

    public static AbstractFactoryInterface getFactory() {
		if (storeType == 0) {
            return new StorageFactory();
        } else if (storeType == 1) {
			return new FileFactory();
		} else if (storeType == 2) {
			return new XmlFactory();
		}else if (storeType == 3) {
			return new JdbcFactory();
		}
        return null;
    }
}
