package LocalNetwork.Factory;

import LocalNetwork.*;
import LocalNetwork.Interfaces.AbstractFactoryInterface;
import LocalNetwork.Interfaces.ComputerServiceInterface;
import LocalNetwork.Interfaces.ServerServiceInterface;
import LocalNetwork.Services.FileComputerService;
import LocalNetwork.Services.FileServerService;

public class FileFactory implements AbstractFactoryInterface {

       public ComputerServiceInterface createComputerService() {
           ComputerServiceInterface fileComputer = new FileComputerService();
           return fileComputer;
       }

    public ServerServiceInterface createServerService() {
        ServerServiceInterface fileServer = new FileServerService();
        return fileServer;
    }
}

