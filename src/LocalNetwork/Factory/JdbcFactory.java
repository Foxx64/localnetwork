package LocalNetwork.Factory;

import LocalNetwork.Interfaces.AbstractFactoryInterface;
import LocalNetwork.Interfaces.ComputerServiceInterface;
import LocalNetwork.Services.JdbcComputerService;
import LocalNetwork.Interfaces.ServerServiceInterface;
public class JdbcFactory implements AbstractFactoryInterface {

    public ServerServiceInterface createServerService() {
        return null;
    }

    public ComputerServiceInterface createComputerService() {
        ComputerServiceInterface computerJdbc = new JdbcComputerService();
        return computerJdbc;
    }
}
