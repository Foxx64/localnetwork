package LocalNetwork.Factory;

import LocalNetwork.Interfaces.AbstractFactoryInterface;
import LocalNetwork.Interfaces.ComputerServiceInterface;
import LocalNetwork.Interfaces.ServerServiceInterface;
import LocalNetwork.Services.XmlComputerService;

public class XmlFactory implements AbstractFactoryInterface {

    public ServerServiceInterface createServerService() {
        return null;
    }

    public ComputerServiceInterface createComputerService() {
        ComputerServiceInterface computerXml = new XmlComputerService();
        return computerXml;
    }
}
