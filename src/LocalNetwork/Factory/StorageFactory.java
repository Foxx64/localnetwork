package LocalNetwork.Factory;

import LocalNetwork.*;
import LocalNetwork.Interfaces.AbstractFactoryInterface;
import LocalNetwork.Interfaces.ComputerServiceInterface;
import LocalNetwork.Interfaces.ServerServiceInterface;
import LocalNetwork.Services.StorageComputerService;
import LocalNetwork.Services.StorageServerService;

public class StorageFactory implements AbstractFactoryInterface {
    public ComputerServiceInterface createComputerService() {
        return new StorageComputerService();
    }
    public ServerServiceInterface createServerService() {
        return new StorageServerService();
    }
}
