package LocalNetwork.Services;

import LocalNetwork.Interfaces.ComputerServiceInterface;
import LocalNetwork.Entitys.Computer;
import LocalNetwork.Exceptions.EntityNotFoundException;
import LocalNetwork.LokalNetwork;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class FileComputerService implements ComputerServiceInterface {
    static Logger logger = Logger.getLogger(FileComputerService.class);

    @Override
    public ArrayList<Computer> getAll() {
//        Computer computer = new Computer();
//        computer.setIp("123.15.18.12");
//        computer.setName("Иванов Иван");
//        computer.setId(0);
//        computer.setOsVersion("Windows 2008");
//        computer.setStorage("5tb");
        ArrayList<Computer> computers = new ArrayList<Computer>();
//        addComputer(computer);
//        computers.add(computer);
        File dir = new File(getComputerPath());
        File[] files = dir.listFiles();
        assert files != null;
        for (File file : files) {
            if (file.isFile()) {
                try {
                    String fileName = file.getAbsolutePath();
                    Path path = Paths.get(fileName);
                    byte[] bytes = java.nio.file.Files.readAllBytes(path);
                    ByteArrayInputStream is = new ByteArrayInputStream(bytes);
                    ObjectInputStream ois = new ObjectInputStream(is);
                    Object computerObject = ois.readObject();
                    Computer computer = (Computer) computerObject;
                    computers.add(computer);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        return computers;
    }

    @Override
    public void addComputer(Computer computer) {
        ArrayList <Computer> computers = getAll();
        int id = getId(computers);
        computer.setId(id);
        updateComputer(computer);
    }

    public int getId(ArrayList<Computer> computers) {
        if (computers.size() == 0) {
            return 0;
        } else {
            if(computers.get(0).getId()== null){
                computers.get(0).setId(0);
                return 0;
            } else {
                int max = computers.get(0).getId();
                for (int i = 1; i < computers.size(); i++) {
                    int itemId = computers.get(i).getId();
                    if (max < itemId) {
                        max = itemId;
                    }
                }
                return max + 1;
            }
        }
    }

    @Override
    public Computer getComputerById(Integer id) throws EntityNotFoundException {
        ArrayList<Computer> computers = getAll();
        Computer computer = null;
        for (Computer computer1 : computers) {
            if (computer1.getId().equals(id)) {
                computer = computer1;
            }
        }
        if (computer == null) {
            logger.error("Компьютер с таким ID не найден");
            throw new EntityNotFoundException();
        }
        return computer;
    }

    @Override
    public Computer getByName(String name) {
        ArrayList<Computer> computers = getAll();
        Computer computer = null;
        for (int i = 0; i <computers.size(); i++) {
            if (computers.get(i).getName().equals(name)) {
                computer = computers.get(i);
            }
        }
        return computer;
    }

    @Override
    public void remove(Computer computer) {
        ArrayList computers = getAll();
        computers.remove(computer);
    }

    @Override
    public void updateComputer(Computer computer) {
        String writeComputer = getComputerPath()+"computer_" + computer.getId() + ".txt";
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(computer);
            byte[] bytes = os.toByteArray();
            java.nio.file.Files.write(Paths.get(writeComputer), bytes);
        } catch (IOException e) {
            logger.error("Ошибка");
        }
    }

    public String getComputerPath() {
        URL location = LokalNetwork.class.getProtectionDomain().getCodeSource().getLocation();
        String binPath = null;
        try {
            File file = new File(location.toURI());
            binPath = file.getPath();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return binPath + "\\..\\Computers\\";
    }
}