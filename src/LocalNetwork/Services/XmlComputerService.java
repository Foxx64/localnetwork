package LocalNetwork.Services;

import LocalNetwork.Interfaces.ComputerServiceInterface;
import LocalNetwork.Entitys.Computer;
import LocalNetwork.Exceptions.EntityNotFoundException;
import LocalNetwork.LokalNetwork;
import LocalNetwork.Storage.Storage;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

public class XmlComputerService implements ComputerServiceInterface {
    static Logger logger = Logger.getLogger(XmlComputerService.class);
    @Override
    public ArrayList<Computer> getAll() {
        File dir = new File(getComputerPath());
        File[] files = dir.listFiles();
        ArrayList<Computer> computers = new ArrayList<Computer>();
        assert files != null;
        for (File file : files) {
            if (file.isFile()) {
                try {
                    FileReader fileReader = new FileReader(file);
                    BufferedReader bufferedReader = new BufferedReader(fileReader);
                    String line;
                    line = bufferedReader.readLine();
                    JAXBContext jaxbContext = JAXBContext.newInstance(Computer.class);
                    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                    Computer computer = (Computer) jaxbUnmarshaller.unmarshal(new StringReader(line));
                    computers.add(computer);
                } catch (FileNotFoundException e) {
                    logger.error("Отсутствует значение!", e);
                    e.printStackTrace();
                } catch (IOException e) {
                    logger.error("Файл не найден", e);
                    e.printStackTrace();
                } catch (JAXBException e) {
                    logger.error("Ошибка десеррилизации файла!", e);
                    e.printStackTrace();
                }
            }
        }
        return computers;
    }

    @Override
    public Computer getComputerById(Integer id) throws EntityNotFoundException {
        ArrayList<Computer> computers = getAll();
        Computer computer = null;
        for (Computer computer1 : computers) {
            if (computer1.getId().equals(id)) {
                computer = computer1;
            }
        }
        if (computer == null) {
                logger.error("Компьютер с таким ID не найден!");
            throw new EntityNotFoundException();
        }
        return computer;
    }

    @Override
    public Computer getByName(String name) {
        Computer computer = null;
        for (int i = 0; i < Storage.getInstance().computers.size(); i++) {
            if (Storage.getInstance().computers.get(i).getName().equals(name)) {
                computer = Storage.getInstance().computers.get(i);
            }
        }
        return computer;
    }

    @Override
    public void remove(Computer computer) {
        ArrayList computers = getAll();
        computers.remove(computer);
    }

    @Override
    public void addComputer(Computer computer){
        ArrayList<Computer> computers = getAll();
        int id = getId(computers);
        computer.setId(id);
        updateComputer(computer);
    }

    @Override
    public void updateComputer(Computer computer) {
        try{ String writeComputer = getComputerPath() + "computer_" + computer.getId() +".txt";
            Marshaller jaxbMarshaller = JAXBContext.newInstance(Computer.class).createMarshaller();
            OutputStream out = new ByteArrayOutputStream(256);
            jaxbMarshaller.marshal(computer, out);
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            String xml = out.toString();
            BufferedWriter bw = new BufferedWriter(new FileWriter(writeComputer));
            bw.write(xml);
            bw.flush();
        } catch (Exception e){
            logger.error("ошибка!", e);
             e.printStackTrace();
        }

    }

    private String getComputerPath() {
        URL location = LokalNetwork.class.getProtectionDomain().getCodeSource().getLocation();
        String binPath = null;
        try {
            File file = new File(location.toURI());
            binPath = file.getPath();
        } catch (URISyntaxException e) {
            logger.error("Недопустимый символ в запросе!", e);
                    e.printStackTrace();
        }
        return binPath + "\\..\\ComputerXml\\";
    }

    public int getId(ArrayList<Computer> computers) {
        if (computers.size() == 0) {
            return 0;
        } else {
            int max = computers.get(0).getId();
            for (int i = 1; i < computers.size(); i++) {
                int itemId = computers.get(i).getId();
                if (max < itemId) {
                    max = itemId;
                }
            }
            return max + 1;
        }
    }
}
