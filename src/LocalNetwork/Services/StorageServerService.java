package LocalNetwork.Services;

import LocalNetwork.Entitys.Computer;
import LocalNetwork.Entitys.Server;
import LocalNetwork.Exceptions.EntityNotFoundException;
import LocalNetwork.LokalNetwork;
import LocalNetwork.Menu.ComputerMenu;
import LocalNetwork.Interfaces.ComputerServiceInterface;
import LocalNetwork.Interfaces.ServerServiceInterface;
import LocalNetwork.Storage.Storage;
import LocalNetwork.Utils.Utils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Scanner;

public class StorageServerService implements ServerServiceInterface {
    static Logger logger = Logger.getLogger(LokalNetwork.class);

    @Override
    public ArrayList<Server> getAll() {

        return Storage.getInstance().servers;
    }

    @Override
    public void addServer(Server server) {

        Storage.getInstance().servers.add(server);
        for (int i = 0; i < Storage.getInstance().servers.size(); i++) {//надо поправить
            Storage.getInstance().servers.get(i).setId(i);
        }
    }

    @Override
    public Server getServerById(Integer id) throws EntityNotFoundException {
            Server server = null;
            for(int m = 0; m < Storage.getInstance().servers.size(); m++){
                if (Storage.getInstance().servers.get(m).getId().equals(id)){
                    server = Storage.getInstance().servers.get(m);
                }
            }
            if (server == null)
            {
                logger.error("Сервер с таким ID не найден");
                throw new EntityNotFoundException();
            }
            return server;
    }

    @Override
    public Server getByName(String name) {
        Server server = null;
        for(int i = 0; i < Storage.getInstance().servers.size(); i++){
            if (Storage.getInstance().servers.get(i).getName().equals(name)){
                server = Storage.getInstance().servers.get(i);
            }
        }
        return server;
    }

    @Override
    public void remove(Server server) {
        ArrayList<Server>servers = getAll();
        servers.remove(server);
    }

    @Override
    public void removeComputer(Server server, Scanner scan) throws EntityNotFoundException {
        ComputerServiceInterface serviceComputer = new StorageComputerService();
        System.out.println("Введите пожалуйста ID компьютера ");
        int computerId = Utils.nextIntLine(scan);
        Computer computer = serviceComputer.getComputerById(computerId);
        logger.info("удаление компьютера из ArrayList");
        server.removeServerComputers(computer);
        System.out.println("Компьютер удален");
    }

    @Override
    public  void addComputer(Server server, int computerId) throws EntityNotFoundException {
        System.out.println("Введите пожалуйста ID компьютера ");
        ComputerServiceInterface service = ComputerMenu.getComputerService();
        Computer computer = service.getComputerById(computerId);
        server.addServerComputers(computer);
        System.out.println("Компьютер добавлен");

    }

    @Override
    public void updateServer(Server server) {
    }
}
