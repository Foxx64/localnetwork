package LocalNetwork.Services;


import LocalNetwork.Interfaces.ComputerServiceInterface;
import LocalNetwork.Entitys.Computer;
import LocalNetwork.Entitys.Server;
import LocalNetwork.Exceptions.EntityNotFoundException;
import LocalNetwork.LokalNetwork;
import LocalNetwork.Interfaces.ServerServiceInterface;
import LocalNetwork.Utils.Utils;
import org.apache.log4j.Logger;

import java.io.*;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class FileServerService implements ServerServiceInterface {
    static Logger logger = Logger.getLogger(LokalNetwork.class);
    @Override
    public ArrayList<Server> getAll() throws IOException, EntityNotFoundException {
        ArrayList<Server>servers = new ArrayList<Server>();
//        Server server = new Server();
//        server.setIp("123.15.18.12");
//        server.setName("Mars");
//        server.setId(0);
//        server.setOsVersion("Windows 2008");
//        server.setStorage("5tb");
//        servers.add(server);
//        addServer(server);
        File dir = new File(getServerPath());
        File[] files = dir.listFiles();
        assert files != null;
        for (File file : files) {
            if (file.isFile()) {
                try {
                    String fileName = file.getAbsolutePath();
                    Path path = Paths.get(fileName);
                    byte[] bytes = java.nio.file.Files.readAllBytes(path);
                    ByteArrayInputStream is = new ByteArrayInputStream(bytes);
                    ObjectInputStream ois = new ObjectInputStream(is);
                    Object serverObject = ois.readObject();
                    Server server = (Server) serverObject;
                    servers.add(server);
                } catch (Exception e ) {
                    logger.error("Ошибка", e);
                    e.printStackTrace();
                }
            }
        }
        return servers;
     }

    @Override
    public void addServer(Server server) throws IOException, EntityNotFoundException {
        ArrayList<Server>servers = getAll();
        int id = getId(servers);
        server.setId(id);
        updateServer(server);
    }

    @Override
    public Server getServerById(Integer id) throws EntityNotFoundException, IOException {
        ArrayList<Server> servers = getAll();
        Server server = null;
        for (Server server1 : servers) {
            if (server1.getId().equals(id)) {
                server = server1;
            }
        }
        if (server == null)
        {
            logger.error("Сервер с таким ID не найден");
            throw new EntityNotFoundException();
        }
        return server;
    }

    @Override
    public Server getByName(String name) throws IOException, EntityNotFoundException {
        ArrayList<Server> servers = getAll();
        Server server = null;
        for(int i = 0; i < servers.size(); i++){
            if (servers.get(i).getName().equals(name)){
                server = servers.get(i);
            }
        }
        return server;
    }

    @Override
    public void remove(Server server) {
        File serverPath = new File(getServerPath() + "server_" + server.getId() + ".txt");
        serverPath.delete();
    }

    @Override
    public void removeComputer(Server server, Scanner scan) throws EntityNotFoundException {
        ComputerServiceInterface serviceComputer = new FileComputerService();
        System.out.println("Введите пожалуйста ID компьютера ");
        int computerId = Utils.nextIntLine(scan);
        Computer computer = serviceComputer.getComputerById(computerId);
        logger.info("удаление компьютера из ArrayList");
        server.removeServerComputers(computer);
        System.out.println("Компьютер удален");

    }

    @Override
    public void addComputer(Server server, int computerId) throws EntityNotFoundException {
        FileComputerService f = new FileComputerService();
        Computer computer =  f.getComputerById(computerId);
        server.addServerComputers(computer);
        System.out.println("Компьютер добавлен");
        updateServer(server);
    }

    @Override
    public void updateServer(Server server) {
        String writeServer = getServerPath()+ "server_" + server.getId() +".txt";
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(server);
            byte[] bytes = os.toByteArray();
            java.nio.file.Files.write(Paths.get(writeServer), bytes);
        } catch (IOException e) {
            logger.error("Ошибка", e);
            e.printStackTrace();
        }
    }

    private String getServerPath() {
        URL location = LokalNetwork.class.getProtectionDomain().getCodeSource().getLocation();
        String binPath = null;
        try {
            File file = new File(location.toURI());
            binPath = file.getPath();
        } catch (URISyntaxException e) {
            logger.error("Ошибка", e);
            e.printStackTrace();
        }
        return binPath + "\\..\\Servers\\";
    }

    public int getId(ArrayList<Server> servers ) {

        if (servers.size() == 0) {
            return 0;
        } else {
           if (servers.get(0).getId() == null){
               servers.get(0).setId(0);
               return 0;
           } else {
               int max = servers.get(0).getId();
               for (int i = 1; i < servers.size(); i++) {
                   int itemId = servers.get(i).getId();
                   if (max < itemId) {
                       max = itemId;
                   }
               }
               return max + 1;
           }
        }
    }
}
