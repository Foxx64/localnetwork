package LocalNetwork.Services;

import LocalNetwork.Interfaces.ComputerServiceInterface;
import LocalNetwork.Entitys.Computer;
import LocalNetwork.Exceptions.EntityNotFoundException;
import LocalNetwork.LokalNetwork;
import LocalNetwork.Storage.Storage;
import org.apache.log4j.Logger;

import java.util.ArrayList;

public class StorageComputerService implements ComputerServiceInterface {

    static Logger logger = Logger.getLogger(StorageComputerService.class);

    @Override
    public ArrayList <Computer>  getAll() {
        return Storage.getInstance().computers;
    }

    @Override
    public void addComputer(Computer computer){
        ArrayList<Computer> computers = getAll();
        FileComputerService fileComputerService = new FileComputerService();
        int id = fileComputerService.getId(computers);
        computer.setId(id);
        Storage.getInstance().computers.add(computer);
    }

    @Override
    public  Computer  getComputerById(Integer id) throws EntityNotFoundException {
        Computer computer = null;
        for (Computer computer1:Storage.getInstance().computers) {
            if (computer1.getId().equals(id)) {
                computer = computer1;
                break;
            }
        }
        if (computer == null)
        {
            logger.error("Компьютер с таким Id не найден");
            throw new EntityNotFoundException();
        }
        return computer;
    }

    @Override
    public Computer getByName(String name) {
        Computer computer = null;
        for(int i = 0; i < Storage.getInstance().computers.size(); i++){
            if (Storage.getInstance().computers.get(i).getName().equals(name)){
                computer = Storage.getInstance().computers.get(i);
            }
        }
        return computer;
    }

    @Override
    public void remove(Computer computer) {
       ArrayList<Computer>computers = getAll();
        computers.remove(computer);
    }

    /**
     * Класс StorageComputerService не работает
     * с записью и чтением из файлов
     * и следовательно обновление файлов здесь не требуется
     * * @param computer
     */
    @Override
    public void updateComputer(Computer computer) {

    }
    public ComputerServiceInterface createComputerService() {
        if (LokalNetwork.storeType == 0) {
            return new StorageComputerService();
        } else if (LokalNetwork.storeType == 1) {
            return new FileComputerService();
        } else if (LokalNetwork.storeType == 2) {
            return new XmlComputerService();
        }else if (LokalNetwork.storeType == 3) {
            return new JdbcComputerService();
        }
        return null;
    }
}
