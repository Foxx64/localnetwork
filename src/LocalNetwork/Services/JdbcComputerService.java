package LocalNetwork.Services;

import LocalNetwork.Interfaces.ComputerServiceInterface;
import LocalNetwork.Entitys.Computer;
import LocalNetwork.Exceptions.EntityNotFoundException;
import LocalNetwork.Storage.Storage;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;

public class JdbcComputerService implements ComputerServiceInterface {
    static Logger logger = Logger.getLogger(JdbcComputerService.class);
    private Connection getConnection() throws SQLException {
        return  DriverManager.getConnection(
                "jdbc:oracle:thin:@localhost:1521:Orcl", "SYS as SYSDBA",
                "123");
    }

    public static void loadDriver(){
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            logger.error("Класс не найден", e);
            e.printStackTrace();
        }
      }

    @Override
    public ArrayList<Computer> getAll(){
        ArrayList<Computer> computers = new ArrayList<Computer>();
        try{
            Connection connection = getConnection();
            Statement st = connection.createStatement();
            String wordQuery = "Select * From Computers";
            ResultSet resultSet = st.executeQuery(wordQuery);
            while (resultSet.next()){
                Computer computer = new Computer();
                String osVersion = resultSet.getString("OSVERSION");
                computer.setOsVersion(osVersion);
                String name = resultSet.getString("NAME");
                computer.setName(name);
                String storage = resultSet.getString("STORAGE");
                computer.setStorage(storage);
                String ip = resultSet.getString("IP");
                computer.setIp(ip);
                int id = resultSet.getInt("ID");
                computer.setId(id);
                computers.add(computer);
            }
            connection.close();
        } catch (SQLException e) {
            logger.error("Ошибка доступа к базе данных!", e);
            e.printStackTrace();
        }
        return computers;
    }

    @Override
    public void addComputer(Computer computer){
        ArrayList<Computer> computers = getAll();
        int id = getId(computers);
        computer.setId(id);
        updateComputer(computer);
    }

    @Override
    public Computer getComputerById(Integer id) throws EntityNotFoundException {
        ArrayList<Computer> computers = getAll();
        Computer computer = null;
        for (Computer computer1 : computers) {
            if (computer1.getId().equals(id)) {
                computer = computer1;
            }
        }
        if (computer == null) {
            logger.error("Компьютер с таким ID не найден!");
            throw new EntityNotFoundException();
        }
        return computer;

    }

    @Override
    public Computer getByName(String name) {
        Computer computer = null;
        for(int i = 0; i < Storage.getInstance().computers.size(); i++){
            if (Storage.getInstance().computers.get(i).getName().equals(name)){
                computer = Storage.getInstance().computers.get(i);
            }
        }
        return computer;
    }

    @Override
    public void remove(Computer computer) {
        try {
            Connection connection = getConnection();
            Statement st = connection.createStatement();
            String query = String.format("delete from computers where ID = %d", computer.getId());
            st.execute(query);
            connection.close();
        } catch (SQLException e) {
            logger.error("Ошибка десеррилизации файла!", e);
            e.printStackTrace();
        }
    }

    @Override
    public void updateComputer(Computer computer) {
        Connection connection;
        try {
            connection = getConnection();
            String query = "insert into computers"
                    + " Values ( '" + computer.getOsVersion() + "', '" + computer.getName() + "', '" + computer.getStorage()
                    +  "', '" +  computer.getIp() + "', '"+ computer.getId()+ "' )";
            Statement st = connection.createStatement();
            st.execute(query);

        } catch (SQLException e) {
            logger.error("Ошибка доступа к базе данных!", e);
            e.printStackTrace();
        }
    }

    public int getId(ArrayList<Computer> computers) {
        if (computers.size() == 0) {
            return 0;
        } else {
            if(computers.get(0).getId()== null){
                computers.get(0).setId(0);
                return 0;
            } else {int max = computers.get(0).getId();
                for (int i = 1; i < computers.size(); i++) {
                    int itemId = computers.get(i).getId();
                    if (max < itemId) {
                        max = itemId;
                    }
                }
                return max + 1;}
        }
    }
}
